# NVIDIA Tegra Buildstream BSP

BSP for tegra platforms, *HEAVILY* copied from https://gitlab.com/celduin/crash/jetbot-system-bst

You need files from the nvidia sdk for this (4.4 DP), this is bassed on a TX2 so:

SDK_PATH=/path/to/tegra-bsp/sdkm_downloads

./sdkmanager --license accept  --cli downloadonly --downloadfolder $SDK_PATH --logs /tmp/dl.log --user $useremail --password $userpass --logintype devzone --product Jetson --version DP_4.4 --targetos Linux --target P3310 --additionalsdk DeepStream

The build a tarball with

bst build deploy/deploy-minimal-systemd-tx2.bsp
bsp checkout deploy/deploy-minimal-systemd-tx2.bsp tarball/

and an image with

bst build deploy/deploy-minimal-systemd-tx2-image.bsp
bst build deploy/deploy-minimal-systemd-tx2-image.bsp images/

